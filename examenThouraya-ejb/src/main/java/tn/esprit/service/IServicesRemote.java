package tn.esprit.service;


import java.util.List;

import javax.ejb.Remote;

import tn.esprit.entites.Article;
import tn.esprit.entites.Categorie;
import tn.esprit.entites.Commande;

@Remote
public interface IServicesRemote {

	void ajouterCategorie(Categorie categorie);

	void ajouterArticle(Article article);

	void ajouterCommande(Commande commande);

	void affecterArtCat(int idArt, int idCat);

	void affecterArticleACommande(int idCmd, int idArt);

	List<String> getAllArticleNamesByCategorie(int idMaCategorie);

	void deleteCategorieById(int categorieId);

	List<Article> getArticlesAvecPrixSuperieurA(int prix);

}