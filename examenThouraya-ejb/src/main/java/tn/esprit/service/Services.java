package tn.esprit.service;

import java.time.Instant;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.stream.Collector;
import java.util.stream.Collectors;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import tn.esprit.entites.Article;
import tn.esprit.entites.Categorie;
import tn.esprit.entites.Commande;

@Stateless
@LocalBean
public class Services implements IServicesRemote {
	
	@PersistenceContext
	EntityManager em;
	
	
	@Override
	public void ajouterCategorie(Categorie categorie){
		em.persist(categorie);
	}

	
	@Override
	public void ajouterArticle(Article article){
		em.persist(article);
	}
	
	
	@Override
	public void ajouterCommande(Commande commande){
		em.persist(commande);
	}
	
	@Override
	public void affecterArtCat(int idArt, int idCat){
		long startTime = Instant.now().toEpochMilli();
		Categorie categorie = new Categorie(idCat);
		Query query = em.createQuery("update Article set categorie=:cat where idArt=:artId");
		query.setParameter("cat", categorie);
		query.setParameter("artId", idArt);
		query.executeUpdate();
		em.flush();
		long endTime = Instant.now().toEpochMilli();
		long elapsedTime = endTime - startTime;
		System.out.println("update query jpql: " + elapsedTime);
	}
	
	@Override
	public void affecterArticleACommande(int idCmd, int idArt){
		long startTime = Instant.now().toEpochMilli();
		Commande commande = em.find(Commande.class, idCmd);
		Article article = em.find(Article.class, idArt);
		commande.setArticles(Arrays.asList(article));
		em.flush();
		long endTime = Instant.now().toEpochMilli();
		long elapsedTime = endTime - startTime;
		System.out.println("update query entity manager methods : " + elapsedTime);
	}
	
	@Override
	public List<String> getAllArticleNamesByCategorie(int idMaCategorie){
		//Pour optimiser la requete, il vaut mieux utiliser JPQL
		Query query = em.createQuery("select art from Article art where art.categorie=:categorie");
		query.setParameter("categorie", new Categorie(idMaCategorie));
		List<Article> articles = query.getResultList();
		return articles.stream()
			.map(Article::getNom)
			.collect(Collectors.toList());
	}
	
	
	
	@Override
	public void deleteCategorieById(int categorieId){
		//il faut tout d'abord supprimer les commandes sur les articles de la catégorie a supprimer
		//Pour comprendre l'erreur essayer de ne pas supprimer les commandes.	
		Query queryselectCommands = em.createQuery(
						"select c from Commande c join c.articles arts where arts in "
						+ "("
						+ "select art from Article art where art.categorie.idCat=:categorieId"
						+ ") "
					);
		queryselectCommands.setParameter("categorieId", categorieId);
		List<Commande> commandes = queryselectCommands.getResultList();
			
			
		if(!commandes.isEmpty()){
			Query queryDeleteCommande = em.createQuery("delete from Commande c where c in (:commandes)");
			queryDeleteCommande.setParameter("commandes", commandes);
			int countDeletedCommandes= queryDeleteCommande.executeUpdate();
			System.out.println("deleted Commandes = " + countDeletedCommandes);				
		}

		em.remove(em.find(Categorie.class, categorieId)); //Cascade doesn't work with JPQL
	}
	
	
	@Override
	public List<Article> getArticlesAvecPrixSuperieurA(int prix){
		Query query = em.createQuery("select a from Article A where a.prix>=:prix");
		query.setParameter("prix", prix);
		return query.getResultList();
	}
	
	
	public List<Categorie> getAllCategorie(){
		return em.createQuery("from Categorie").getResultList();
	}
	
	
	
	
}