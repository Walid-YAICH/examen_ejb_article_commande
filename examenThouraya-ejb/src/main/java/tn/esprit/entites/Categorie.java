package tn.esprit.entites;

import java.io.Serializable;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;

@Entity
public class Categorie implements Serializable{

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int idCat;
	
	
	private String nomCat;
	
	@OneToMany(mappedBy="categorie", cascade=CascadeType.REMOVE)
	private List<Article> articles;

	public Categorie() {
	}
	
	
	
	public Categorie(int idCat) {
		super();
		this.idCat = idCat;
	}



	public Categorie(String nomCat) {
		super();
		this.nomCat = nomCat;
	}

	public int getIdCat() {
		return idCat;
	}

	public void setIdCat(int idCat) {
		this.idCat = idCat;
	}

	public String getNomCat() {
		return nomCat;
	}

	public void setNomCat(String nomCat) {
		this.nomCat = nomCat;
	}

	public List<Article> getArticles() {
		return articles;
	}

	public void setArticles(List<Article> articles) {
		this.articles = articles;
	}



	@Override
	public String toString() {
		return nomCat ;
	}
	
	
	
}
