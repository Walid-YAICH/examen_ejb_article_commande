package tn.esprit.entites;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

@Entity
public class Article implements Serializable{

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int idArt;
	
	private String nom;
	
	private int prix;
	
	private String image;
	
	@ManyToOne
	private Categorie categorie;

	
	public Article() {
	}
	
	
	
	public Article(int idArt) {
		super();
		this.idArt = idArt;
	}



	public Article(String nom, int prix, String image) {
		super();
		this.nom = nom;
		this.prix = prix;
		this.image = image;
	}

	public int getIdArt() {
		return idArt;
	}

	public void setIdArt(int idArt) {
		this.idArt = idArt;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public int getPrix() {
		return prix;
	}

	public void setPrix(int prix) {
		this.prix = prix;
	}

	public String getImage() {
		return image;
	}

	public void setImage(String image) {
		this.image = image;
	}

	public Categorie getCategorie() {
		return categorie;
	}

	public void setCategorie(Categorie categorie) {
		this.categorie = categorie;
	}



	@Override
	public String toString() {
		return "Article [idArt=" + idArt + ", nom=" + nom + ", prix=" + prix + ", image=" + image + "]";
	}
	
	
	
	
	
	
}
