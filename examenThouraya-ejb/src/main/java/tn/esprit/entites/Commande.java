package tn.esprit.entites;
import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;

@Entity
public class Commande implements Serializable {

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int idCmd;
	
	private Date date;
	
	private Status status;
	
	public Commande() {
	}
	
	
	public Commande(int idCmd) {
		super();
		this.idCmd = idCmd;
	}



	public Commande(Date date, Status status) {
		super();
		this.date = date;
		this.status = status;
	}

	@OneToMany
	private List<Article> articles;

	public int getIdCmd() {
		return idCmd;
	}

	public void setIdCmd(int idCmd) {
		this.idCmd = idCmd;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public Status getStatus() {
		return status;
	}

	public void setStatus(Status status) {
		this.status = status;
	}

	public List<Article> getArticles() {
		return articles;
	}

	public void setArticles(List<Article> articles) {
		this.articles = articles;
	}


	@Override
	public String toString() {
		return "Commande [idCmd=" + idCmd + ", date=" + date + ", status=" + status + "]";
	}
	
	
	
	
}
