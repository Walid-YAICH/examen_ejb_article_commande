package tn.esprit.managedBean;

import java.util.List;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;

import tn.esprit.entites.Article;
import tn.esprit.entites.Categorie;
import tn.esprit.service.Services;

@ManagedBean
public class GestionArticle {

	@EJB
	Services services;
	
	private String nom;
	
	private int prix;
	
	private String image;
	
	List<Categorie> categories;
	
	private int selectedCategorieId;
	
	@PostConstruct
	void init(){
		categories = services.getAllCategorie();
	}
	
	public void ajouterArticle(){
		Article article = new Article(nom, prix, image);
		Categorie categorie = new Categorie(selectedCategorieId);
		article.setCategorie(categorie);
		services.ajouterArticle(article);
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public int getPrix() {
		return prix;
	}

	public void setPrix(int prix) {
		this.prix = prix;
	}

	public String getImage() {
		return image;
	}

	public void setImage(String image) {
		this.image = image;
	}

	public List<Categorie> getCategories() {
		return categories;
	}

	public void setCategories(List<Categorie> categories) {
		this.categories = categories;
	}

	public int getSelectedCategorieId() {
		return selectedCategorieId;
	}

	public void setSelectedCategorieId(int selectedCategorieId) {
		this.selectedCategorieId = selectedCategorieId;
	}
	
	
	
}
