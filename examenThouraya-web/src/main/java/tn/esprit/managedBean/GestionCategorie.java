package tn.esprit.managedBean;

import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;

import tn.esprit.entites.Categorie;
import tn.esprit.service.Services;

@ManagedBean
public class GestionCategorie {

	@EJB
	Services services;
	
	private String nom;	
	
	public void ajouterCategorie(){
		services.ajouterCategorie(new Categorie(nom));
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}
	
	
	
}
